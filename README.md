# README #

 Разработать воркер-движок парсинга результатов поисковых систем (Google, Bing, Yahoo)
 Требования: расширяемость, добавление новых поисковых систем, работа через прокси, оптимальное решение по потреблению памяти.
 Запуск через cli, пример php cli.php -a search-results.google -p keyword='hello world'
 Вывод результатов в следующем виде:
 all_resuts - 2220000
 position1, domain_name, url
 position2, domain_name, url
 position3, domain_name, url
 
 Usage: php crawler.php -a search-results.google -p keyword='hello world' --proxy-ip 192.168.24.123 --proxy-port 8080
 
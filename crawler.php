<?php
/**
 * Разработать воркер-движок парсинга результатов поисковых систем (Google, Bing, Yahoo)
 * Требования: расширяемость, добавление новых поисковых систем, работа через прокси, оптимальное решение по потреблению памяти.
 * Запуск через cli, пример php cli.php -a search-results.google -p keyword='hello world'
 *
 * Вывод результатов в следующем виде:
 * all_resuts - 2220000
 * position1, domain_name, url
 * position2, domain_name, url
 * position3, domain_name, url
 */

require_once __DIR__ . '/src/phpQuery/phpQuery/phpQuery.php';

/** автолоадер */
spl_autoload_register(function ($class) {
	$path = __DIR__ . '/src/' . $class . '.php';
	if (file_exists($path)) {
		require_once $path;
	}
});

/**
 * Функция проверяет входные параметры переданные в командной строке
 *
 * @param array $options массив параметров из командной строки
 * @return array|bool массив с проверенными данными или false в случае ошибки
 */
function validateOptions($options) {

	$searchEnginePrefix = 'search-results.';
	if (isset($options['a']) && strpos($options['a'], $searchEnginePrefix) === 0) {
		$engine = substr($options['a'], mb_strlen($searchEnginePrefix));
	} else {
		echo "Bad request, key [a]\n";
		return false;
	}

	$searchKeywordPrefix = 'keyword=';
	if (isset($options['p']) && strpos($options['p'], $searchKeywordPrefix) === 0) {
		$keyword = substr($options['p'], mb_strlen($searchKeywordPrefix));
	} else {
		echo "Bad request, key [p]\n";
		return false;
	}

	if (isset($options['proxy-ip'], $options['proxy-port'])
		&& filter_var($options['proxy-ip'], FILTER_VALIDATE_IP) && intval($options['proxy-port'])
	) {
		echo sprintf('Proxy %s:%s enabled', $options['proxy-ip'], $options['proxy-port']) . "\n";
	} else {
		$options['proxy-ip'] = false;
		$options['proxy-port'] = false;
	}

	return [
		'engine' => $engine,
		'keyword' => $keyword,
		'proxy-ip' => $options['proxy-ip'],
		'proxy-port' => $options['proxy-port']
	];
}

/**
 * Функция для печати подсказки, если введенные пармаетры не валидны
 */
function printUsage() {
	echo "\tUsage: php crawler.php -a search-results.google -p keyword='hello world' --proxy-ip 192.168.24.123 --proxy-port 8080\n";
	echo "\tOptional --proxy-ip 192.168.24.123 AND --proxy-port 8080\n";
}

/**
 * Начало работы скрипта
 */

$options = validateOptions(getopt('a:p:', ['proxy-ip:', 'proxy-port:']));

if (!$options) {
	printUsage();
	exit;
}

$searchCrawler = SearchCrawler::getInstance($options);
$searchCrawler->run();
exit;
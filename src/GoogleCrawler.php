<?php

/**
 * Клас парсит результаты поискового запроса Google
 */
class GoogleCrawler extends SearchCrawler {

	/** url шаблон для поиска */
	const QUERY_URL = 'https://www.google.ru/search?q=%s&start=%d';
	/** максимальное число результатов */
	const MAX_RESULTS = 50;

	/**
	 * Запуск парсера
	 */
	public function run() {
		echo "Start Google parser\n";
		for ($linkFrom = 0; $linkFrom < self::MAX_RESULTS; $linkFrom += 10) {
			$url = sprintf(self::QUERY_URL, strtr($this->keyword, ' ', '+'), $linkFrom);
			echo $url . "\n";
			$response = $this->getPageHtml($url);
			$this->parseHtml($response);
			sleep(1);
		}
		curl_close($this->curl);
		echo "Finish Google parser\n\n";
		$this->printResult();

		return $this;
	}

	/**
	 * Метод парсит html код, забирая из него нужную информацию
	 *
	 * @param $html string html код
	 * @return bool boolean успешность парсинга
	 */
	protected function parseHtml($html) {
		try {
			$pq = phpQuery::newDocument($html);
			$links = $pq->find('#res')->find('h3.r')->find('a');
			if (!$this->resultCounts) {
				$resultCounts = $pq->find('#resultStats')->text();
				preg_match('#Результатов:\sпримерно\s([^(]+)#i', $resultCounts, $matches);
				$this->resultCounts = isset($matches[1]) ? $matches[1] : '';
			}
			foreach ($links as $link) {
				$pqLink = pq($link);
				$url = $pqLink->attr('href');
				preg_match('#https?:\/\/([^\/]+)\/#i', $url, $matches);
				$this->result[] = [
					'domain' => isset($matches[1]) ? $matches[1] : '',
					'url' => $url,
					'text' => $pqLink->text(),
				];
			}
		} catch (Exception $e) {
			echo 'Error, ' . $e->getMessage();
			return false;
		}
		return true;
	}

}
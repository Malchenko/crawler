<?php

/**
 * Базовый класс для работы с парсерами
 *
 * Для добавление нового парсера необходимо:
 * 1. Создать класс-наследник реализуюший два метод run() и parseHtml()
 * 2. Добавить его в фабрику SearchCrawler::getInstance()
 *
 */
abstract class SearchCrawler {

	protected $keyword;
	protected $curl;
	protected $result = [];
	protected $resultCounts = '';

	/**
	 * Запуск парсера, в реализации необходима вся логика работы парсера
	 */
	abstract public function run();

	/**
	 * Метод парсинга, в реализации которого html код парсится,
	 * и забирая из него нужную информация в переменные
	 * $this->resultCounts строка содержащая количество найденных результатов
	 * $this->result массив с необходимыми данными(позиция, домен, урл)
	 *
	 * @param $html string html код
	 */
	abstract protected function parseHtml($html);

	/**
	 * Небольшая фабрика для объектов парсеров
	 *
	 * @param $options
	 * @return SearchCrawler
	 */
	public static function getInstance($options) {

		switch ($options['engine']) {
			case 'google':
				$engine = new GoogleCrawler();
				$engine->init($options);
				break;
			default:
				throw new UnexpectedValueException('Invalid engine class name ' . $options['engine']);
				break;
		}

		return $engine;
	}

	/**
	 * Метод инициализируюший необходимую инфраструктуру для работы парсера
	 *
	 * @param $options array массив с настройками
	 * @return $this
	 */
	public function init($options) {
		$this->keyword = $options['keyword'];

		$this->curl = curl_init();
		if ($options['proxy-ip'] && intval($options['proxy-port'])) {
			curl_setopt($this->curl, CURLOPT_PROXY, $options['proxy-ip']);
			curl_setopt($this->curl, CURLOPT_PROXYPORT, intval($options['proxy-port']));
		}
		curl_setopt_array($this->curl, [
			CURLOPT_CONNECTTIMEOUT => 1,
			CURLOPT_TIMEOUT => 3,
			CURLOPT_HEADER => 0,
			CURLOPT_NOBODY => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_USERAGENT => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_MAXREDIRS => 3,
			CURLOPT_HTTPHEADER => [
				'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
				'Accept-Language' => 'en-US,en;q=0.5',
				'Accept-Encoding' => 'gzip, deflate, br',
				'Referer' => 'https://www.google.ru/',
				'Cookie' => 'NID=109=wLzh1EmUN_bYhsRUomMrOzlHNa4UHPadxOcakSUMVvlzc7zgy1NP0cTxbsKJwKrb2eZVctYhlit5o6BYIF-jQ9dLSp_lN35cAZFvVzbOgS6OhlOkqbBaXyDarz7hiAK-3kCqt4Y; DV=8xS6x82ahOgR4GqOzSL9ycpjOFXA3RU; OGPC=5061821-2:; OGP=-5061821:',
				'Connection' => 'keep-alive',
				'Upgrade-Insecure-Requests' => '1'
			],
		]);

		return $this;
	}

	/**
	 * Метод делает курлом GET запрос на указанный url, возврашает тело ответа
	 *
	 * @param $url string целевой url
	 * @return string тело ответа
	 */
	protected function getPageHtml($url) {
		curl_setopt($this->curl, CURLOPT_URL, $url);
		$response = curl_exec($this->curl);
		if (!$response) {
			echo sprintf("Curl error [%d], %s\n", curl_errno($this->curl), curl_error($this->curl));
		}
		return $response;
	}

	/**
	 * Метод выводит результат в подготовленном виде
	 */
	protected function printResult() {
		if ($this->resultCounts) {
			echo "Results:\n";
			echo sprintf("all_resuts - %s\n", $this->resultCounts);

			foreach ($this->result as $index => $resultArray) {
				echo sprintf("position%d, %s, %s\n", ($index + 1), $resultArray['domain'], $resultArray['url']);
			}
		}

		return $this;
	}
}
